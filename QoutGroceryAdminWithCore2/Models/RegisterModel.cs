﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QoutGroceryAdminWithCore2.Models
{
    public class RegisterModel
    {
        public string Area { get; set; }
        [Display(Name = "Area")]
        [Required(ErrorMessage = "Please select your Area.")]
        public int AreaId { get; set; }
        public IEnumerable<SelectListItem> Areas { get; set; }
        public string Avenue { get; set; }
        [Display(Name = "Date of Birth")]
        [RegularExpression("^(([0]\\d|[1][0-2])\\-([0-2]\\d|[3][0-1])\\-[1-2][0-9]\\d{2})$", ErrorMessage = "Please enter in 'mm-dd-yyyy' format.")]
        [Required(ErrorMessage = "Please enter your birth date.")]
        public string BirthDate { get; set; }
        public string Block { get; set; }
        [EmailAddress(ErrorMessage = "Please enter correct email-id.")]
        [Required(ErrorMessage = "Please nenter your email address.")]
        [Remote("EmailExists", "UserManagement", ErrorMessage = "Can't add what already exists!")]
        public string Email { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter your first name.")]
        public string FirstName { get; set; }
        public string Gender { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please select your gender.")]
        public int GenderId { get; set; }
        public IEnumerable<SelectListItem> Genders { get; set; }
        [Display(Name = "House Number")]
        public string HouseNumber { get; set; }
        public string HouseType { get; set; }
        //[Display(Name = "House Type")]
       // [Required(ErrorMessage = "Houser type is required.")]
        public int HouseTypeId { get; set; }
        public IEnumerable<SelectListItem> HouseTypes { get; set; }
        public int IsActive { get; set; }
        public int AddressID { get; set; }
        public int IsDelete { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter your last name.")]
        public string LastName { get; set; }
        [Display(Name = "Mobile Number")]
        [Phone]
        [RegularExpression("^\\d{10}$", ErrorMessage = "Not a mobile number")]
        [Required(ErrorMessage = "please enter your mobile number.")]
        public string MobileNumber { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        [Display(Name = "Nationality")]
        [Required(ErrorMessage = "Please enter your nationality.")]
        public int UserId { get; set; }
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "Please enter your username.")]

        public int NationalityId { get; set; }
        public IEnumerable<SelectListItem> Nationalitys { get; set; }
        [Display(Name = "Address Note")]
        public string Notes { get; set; }
        [Display(Name = "Password")]
       // [RequiredIf("UserId", ErrorMessage = "Please enter your password.")]
        [Required(ErrorMessage = "Please enter your password.")]
        public string Password { get; set; }
        public string Street { get; set; }

        public string UserName { get; set; }
    }

    public class RequiredIfAttribute : RequiredAttribute
    {
        private String PropertyName { get; set; }
        private object[] DesiredValue { get; set; }

        public RequiredIfAttribute(String propertyName)
        {
            PropertyName = propertyName;

        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            Object instance = context.ObjectInstance;
            Type type = instance.GetType();
            Object proprtyvalue = type.GetProperty(PropertyName).GetValue(instance, null);


            if ((int)proprtyvalue == 0)
            {
                ValidationResult result = base.IsValid(value, context);
                return result;
            }



            return ValidationResult.Success;
        }
    }
}
