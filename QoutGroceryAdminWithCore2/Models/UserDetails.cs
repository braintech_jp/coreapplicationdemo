﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QoutGroceryAdminWithCore2.Models
{
    public class UserDetail
    {
        public string UserName { get; set; }
        public string MobileNumber { get; set; }
     
        public List<RegisterModel> UserDetails { get; set; }
    
    }

    
}
