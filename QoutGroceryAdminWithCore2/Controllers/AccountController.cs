﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QoutGroceryAdminWithCore2.Models;
using ApplicationCore.Interfaces;
using ApplicationCore.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace QoutGroceryAdminWithCore2.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILoginService _loginService;
        public AccountController(ILoginService loginService) => _loginService = loginService;

        public IActionResult Login()
        {
            return View();
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login");
        }


        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var _userData = _loginService.UserLogin(new UserEntity() { UserName = model.Username, Password = model.Password });

            if (_userData != null)
            {


                ClaimsIdentity identity = new ClaimsIdentity(new[]
                   {
                        new Claim(ClaimTypes.Name,_userData.Name),

                    new Claim(ClaimTypes.Role,"Admin"),

                    new Claim(ClaimTypes.PrimarySid,_userData.Id.ToString())

                    }, CookieAuthenticationDefaults.AuthenticationScheme);

                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return RedirectToAction("UserDetails", "UserManagement");
            }
            else
            {
                TempData["notFound"] = true;

                return View();
            }

        }
    }
}