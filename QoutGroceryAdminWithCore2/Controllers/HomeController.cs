﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QoutGroceryAdminWithCore2.Models;
using ApplicationCore.Interfaces;
using ApplicationCore.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace QoutGroceryAdminWithCore2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILoginService _loginService;
        public HomeController(ILoginService loginService) => _loginService = loginService;
        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(LoginModel model)
        {
            var _userData = _loginService.UserLogin(new UserEntity() { UserName = model.Username, Password = model.Password });

            if (_userData != null)
            {
                ClaimsIdentity identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.PrimarySid, "Admin"),
                new Claim(ClaimTypes.Role, "Admin") }, CookieAuthenticationDefaults.AuthenticationScheme);
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                return RedirectToAction("About");
            }
            else
            {
                TempData["notFound"] = true;

                return View();
            }

        }

        [Authorize(Roles = "Admin")]
        //[Authorize]
        public IActionResult About()
        {
            ViewData["Message"] = "Welcome Admin.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
