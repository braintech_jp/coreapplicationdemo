﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using QoutGroceryAdminWithCore2.Models;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using QoutGroceryAdminWithCore2.Helper;
using System.Globalization;
using ApplicationCore.Entities;
using Microsoft.Extensions.Configuration;

namespace QoutGroceryAdminWithCore2.Controllers
{
    public class UserManagementController : Controller
    {
        private readonly ICommonService _commonService;

        private readonly IUserManagementService _userService;

        public UserManagementController(ICommonService commonService, IUserManagementService userService)
        {
            _commonService = commonService;
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Register(RegisterModel model)
        {

            if (ModelState.IsValid)
            {
                UserEntity entity = new UserEntity()
                {
                    UserId = model.UserId,
                    AddressID = model.AddressID,
                    UserName = model.UserName,
                    Password = model.Password,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    NationalityId = model.NationalityId,
                    GenderId = model.GenderId,
                    MobileNumber = model.MobileNumber,
                    BirthDate = DateTime.ParseExact(model.BirthDate, "MM-dd-yyyy", CultureInfo.InvariantCulture),
                    AreaId = model.AreaId,
                    Block = model.Block,
                    Avenue = model.Avenue,
                    Street = model.Street,
                    HouseTypeId = model.HouseTypeId,
                    HouseNumber = model.HouseNumber,
                    Notes = model.Notes
                };

                try
                {
                    if (_userService.CreateNewUser(entity))
                    {
                        if (model.UserId > 0)
                        {
                            TempData["Message"] = "User Informaion Updated Successfully.";
                        }
                        else
                        {
                            TempData["Message"] = "User Registered Successfully.";
                        }
                    }
                    else
                    {
                        TempData["Message"] = "User already exists.";
                    }
                }
                catch (Exception ex)
                {
                    //Transaction.Current.Rollback();
                    TempData["Message"] = "User not register,Please try after some time.";
                }
            }
            if (model.UserId > 0)
                return RedirectToAction("UserDetails", "UserManagement");
            return RedirectToAction("Register", "UserManagement");

        }

        [Authorize(Roles = "Admin")]
        public IActionResult UserDetails()
        {
            /// ViewBag.Message = TempData["Message"];
            return View(_userService.GetAllUsersDetail());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> Register(int userId = 0)
        {
            var model = new RegisterModel()
            {

                Nationalitys = (await _commonService.UserNationalityList()).Select(x => new SelectListItem
                {
                    Text = x.Nationality,
                    Value = x.Id.ToString()
                }
               ).ToList(),

                Genders = new List<SelectListItem>() {
                    new SelectListItem() { Text=Common.Gender.Male.ToString(),Value=((int)Common.Gender.Male).ToString() },
                    new SelectListItem() { Text=Common.Gender.Female.ToString(),Value=((int)Common.Gender.Female).ToString() }
                },

                Areas = (await _commonService.UserAreaList()).Select(x => new SelectListItem
                {

                    Text = x.AreaName,
                    Value = x.Id.ToString()

                }).ToList(),

                HouseTypes = (await _commonService.UserHouseTypeList()).Select(x => new SelectListItem
                {
                    Text = x.HouseType,
                    Value = x.Id.ToString()
                }
                ).ToList()

            };

            if (userId > 0)
            {
                var res = _userService.GetUserDetail(userId);
                if (res != null)
                {
                    model.UserId = res.UserId;
                    model.FirstName = res.FirstName;
                    model.LastName = res.LastName;
                    model.UserName = res.UserName;
                    model.Password = res.Password;
                    model.Email = res.Email;
                    model.NationalityId = res.NationalityId;
                    model.GenderId = res.GenderId;
                    model.MobileNumber = res.MobileNumber;
                    model.BirthDate = res.BirthDate.ToString("MM-dd-yyyy");
                    model.AddressID = res.AddressID;
                    model.AreaId = res.AreaId;
                    model.Block = res.Block;
                    model.Avenue = res.Avenue;
                    model.Street = res.Street;
                    model.HouseTypeId = res.HouseTypeId;
                    model.HouseNumber = res.HouseNumber;
                    model.Notes = res.Notes;
                }
            }


            //  ViewBag.Message = TempData["Messsage"];

            return View(model);


        }



        public JsonResult EmailExists(string email)
        {
            return Json(!String.Equals(email, "ehsansajjad@yahoo.com", StringComparison.OrdinalIgnoreCase));
        }
    }
}