﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;

namespace QoutGroceryAdminWithCore2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                //It used for validate anti forgery token only for post method
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddDbContext<QuotGroceryContext>(options =>
          options.UseSqlServer(Configuration.GetConnectionString("QuotGroceryConnection")));

            ////new added
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.AccessDeniedPath = "";
                        options.LoginPath = "/Account/Login";
                        options.LogoutPath = "/Home/Logout";
                    });

            services.AddAuthorization(options =>

            options.AddPolicy("MustBeAuthorixe", x => x.RequireAuthenticatedUser().RequireRole("Admin"))
            );
            ////upto here

            services.AddScoped(typeof(IRepository<>), typeof(EFRepositoryService<>));
            services.AddScoped(typeof(IAsyncRepository<>), typeof(EFRepositoryService<>));
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IUserManagementService, UserManagementService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                
                app.UseDatabaseErrorPage();
            }
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}


            ///New Added
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            //Use a packge NwebSec.AspNetCore.Midleware
            app.UseHsts(options => options.MaxAge(days: 365).IncludeSubdomains());
            app.UseStaticFiles();
            app.UseXXssProtection(options => options.EnabledWithBlockMode());
            app.UseXContentTypeOptions();
            app.UseAuthentication();
            //////////////
            // app.UseStaticFiles();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
