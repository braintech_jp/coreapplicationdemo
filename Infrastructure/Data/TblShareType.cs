﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblShareType : BaseEntity
    {
        public TblShareType()
        {
            TblShare = new HashSet<TblShare>();
            TblSharedByMobileNumber = new HashSet<TblSharedByMobileNumber>();
        }

        public int ShareTypeId { get; set; }
        public string ShareType { get; set; }

        public ICollection<TblShare> TblShare { get; set; }
        public ICollection<TblSharedByMobileNumber> TblSharedByMobileNumber { get; set; }
    }
}
