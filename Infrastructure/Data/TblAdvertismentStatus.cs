﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdvertismentStatus : BaseEntity
    {
        public int AdvertismentStatusId { get; set; }
        public int? AdvertisementId { get; set; }
        public DateTime? HitDate { get; set; }
        public int? HitUserId { get; set; }
        public int? GroceryId { get; set; }
        public int? AdvertismentLocationId { get; set; }

        public TblAdvertisment Advertisement { get; set; }
        public TblAdvertismentLocation AdvertismentLocation { get; set; }
    }
}
