﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblMessage : BaseEntity
    {
        public int MessageId { get; set; }
        public string MessageKey { get; set; }
        public string MessageEn { get; set; }
        public string MessageAr { get; set; }
    }
}
