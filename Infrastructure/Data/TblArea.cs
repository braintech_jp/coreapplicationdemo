﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;


namespace Infrastructure.Data
{
    public partial class TblArea : BaseEntity
    {
        public int AreaId { get; set; }
        public string AreaNameAr { get; set; }
        public string AreaNameEn { get; set; }
        public int? CityId { get; set; }
    }
}
