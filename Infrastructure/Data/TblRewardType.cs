﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblRewardType : BaseEntity
    {
        public TblRewardType()
        {
            TblRewards = new HashSet<TblRewards>();
        }

        public int RewarTypeId { get; set; }
        public string RewardType { get; set; }

        public ICollection<TblRewards> TblRewards { get; set; }
    }
}
