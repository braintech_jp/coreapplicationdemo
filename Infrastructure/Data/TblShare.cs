﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblShare : BaseEntity
    {
        public int ShareId { get; set; }
        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public DateTime ShareDateTime { get; set; }
        public int ShareTypeId { get; set; }
        public int? CartIdListId { get; set; }

        public TblShareType ShareType { get; set; }
    }
}
