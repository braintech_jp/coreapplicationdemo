﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblRole : BaseEntity
    {
        public TblRole()
        {
            TblAdminUser = new HashSet<TblAdminUser>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<TblAdminUser> TblAdminUser { get; set; }
    }
}
