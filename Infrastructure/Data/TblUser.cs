﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblUser : BaseEntity
    {
        public TblUser()
        {
            TblOrders = new HashSet<TblOrders>();
            TblRewardAssigned = new HashSet<TblRewardAssigned>();
            TblShoppingCart = new HashSet<TblShoppingCart>();
            TblShoppingList = new HashSet<TblShoppingList>();
            TblUserAddress = new HashSet<TblUserAddress>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public int? GenderId { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? StatusId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? IsDeleted { get; set; }
        public int? MobileVerificationCode { get; set; }
        public int? IsAdvertismentShow { get; set; }
        public short? Rating { get; set; }
        public int? PreferredLanguageId { get; set; }
        public string Notes { get; set; }
        public string DeviceToken { get; set; }
        public int? IsGuestUser { get; set; }
        public int? DeviceTypeId { get; set; }
        public bool? IsLogedIn { get; set; }
        public int? NationalityId { get; set; }
        public int? NotifyLanguageId { get; set; }

        public TblDeviceType DeviceType { get; set; }
        public TblGender Gender { get; set; }
        public TblPreferredLanguage PreferredLanguage { get; set; }
        public TblStatus Status { get; set; }
        public ICollection<TblOrders> TblOrders { get; set; }
        public ICollection<TblRewardAssigned> TblRewardAssigned { get; set; }
        public ICollection<TblShoppingCart> TblShoppingCart { get; set; }
        public ICollection<TblShoppingList> TblShoppingList { get; set; }
        public ICollection<TblUserAddress> TblUserAddress { get; set; }
    }
}
