﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblNotificationType : BaseEntity
    {
        public TblNotificationType()
        {
            TblNotification = new HashSet<TblNotification>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<TblNotification> TblNotification { get; set; }
    }
}
