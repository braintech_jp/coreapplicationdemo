﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblRewardCartProduct : BaseEntity
    {
        public int RewardCartProductId { get; set; }
        public int RewardCartId { get; set; }
        public int ProductId { get; set; }
        public int IsDeleted { get; set; }
        public int? Quantity { get; set; }

        public TblRewardCart RewardCart { get; set; }
    }
}
