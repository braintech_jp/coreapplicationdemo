﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblUserAddress : BaseEntity
    {
        public int UserAddressId { get; set; }
        public int? UserId { get; set; }
        public string Block { get; set; }
        public string Avenue { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string Notes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? IsDeleted { get; set; }
        public bool? DefaultAddress { get; set; }
        public int? HouseTypeId { get; set; }
        public int? AreaId { get; set; }

        public TblUser User { get; set; }
    }
}
