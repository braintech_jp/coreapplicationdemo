﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblShoppingListProduct : BaseEntity
    {
        public int ShoppingListProductId { get; set; }
        public int? ShoppingListId { get; set; }
        public int? ProductId { get; set; }
        public DateTime? AddedOn { get; set; }
        public int? Isdeleted { get; set; }

        public TblShoppingList ShoppingList { get; set; }
    }
}
