﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblOrderType : BaseEntity
    {
        public TblOrderType()
        {
            TblOrders = new HashSet<TblOrders>();
        }

        public int OrderTypeId { get; set; }
        public string OrderType { get; set; }
        public string OrderTypeAr { get; set; }

        public ICollection<TblOrders> TblOrders { get; set; }
    }
}
