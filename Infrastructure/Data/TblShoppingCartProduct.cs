﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblShoppingCartProduct : BaseEntity
    {
        public int ShoppingCartProductId { get; set; }
        public int? ShoppingCartId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public string Notes { get; set; }
        public int? CollectionStatusId { get; set; }
        public int? OrderId { get; set; }
        public int? OrderStatus { get; set; }
        public decimal? UnitPrice { get; set; }
        public DateTime? AddedOn { get; set; }
        public decimal? SubTotal { get; set; }
        public string UnitAr { get; set; }
        public string UnitEn { get; set; }
        public string PackageNameAr { get; set; }
        public string PackageNameEn { get; set; }
        public decimal? UnitValue { get; set; }

        public TblCollectionStatus CollectionStatus { get; set; }
        public TblOrders Order { get; set; }
        public TblShoppingCart ShoppingCart { get; set; }
    }
}
