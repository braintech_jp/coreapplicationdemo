﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdvertismentCreates : BaseEntity
    {
        public int AdvertismentCreatesId { get; set; }
        public int AdvertismentId { get; set; }
        public string StartAge { get; set; }
        public string EndAge { get; set; }
        public int? NationalityId { get; set; }
        public int? GenderId { get; set; }
        public DateTime? CreatedOn { get; set; }

        public TblAdvertisment Advertisment { get; set; }
    }
}
