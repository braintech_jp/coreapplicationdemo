﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblSentSmsresponse : BaseEntity
    {
        public int ResponseId { get; set; }
        public string MobileNumber { get; set; }
        public string SendMessage { get; set; }
        public DateTime? SendDateTime { get; set; }
        public string ResponseMessage { get; set; }
        public bool? Result { get; set; }
        public decimal? NetPoints { get; set; }
        public string MessageId { get; set; }
    }
}
