﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Infrastructure.Data
{
    public partial class QuotGroceryContext : DbContext
    {
        public QuotGroceryContext(DbContextOptions<QuotGroceryContext> options): base(options)
        {

        }

        public virtual DbSet<TblAdminUser> TblAdminUser { get; set; }
        public virtual DbSet<TblAdvertiser> TblAdvertiser { get; set; }
        public virtual DbSet<TblAdvertiserType> TblAdvertiserType { get; set; }
        public virtual DbSet<TblAdvertisment> TblAdvertisment { get; set; }
        public virtual DbSet<TblAdvertismentCreates> TblAdvertismentCreates { get; set; }
        public virtual DbSet<TblAdvertismentLocation> TblAdvertismentLocation { get; set; }
        public virtual DbSet<TblAdvertismentStatus> TblAdvertismentStatus { get; set; }
        public virtual DbSet<TblArea> TblArea { get; set; }
        public virtual DbSet<TblChangedBy> TblChangedBy { get; set; }
        public virtual DbSet<TblCollectionStatus> TblCollectionStatus { get; set; }
        public virtual DbSet<TblDeviceType> TblDeviceType { get; set; }
        public virtual DbSet<TblGender> TblGender { get; set; }
        public virtual DbSet<TblHouseType> TblHouseType { get; set; }
        public virtual DbSet<TblMessage> TblMessage { get; set; }
        public virtual DbSet<TblNationality> TblNationality { get; set; }
        public virtual DbSet<TblNotification> TblNotification { get; set; }
        public virtual DbSet<TblNotificationType> TblNotificationType { get; set; }
        public virtual DbSet<TblOrderAddress> TblOrderAddress { get; set; }
        public virtual DbSet<TblOrderChangeType> TblOrderChangeType { get; set; }
        public virtual DbSet<TblOrderLogged> TblOrderLogged { get; set; }
        public virtual DbSet<TblOrders> TblOrders { get; set; }
        public virtual DbSet<TblOrderStatus> TblOrderStatus { get; set; }
        public virtual DbSet<TblOrderType> TblOrderType { get; set; }
        public virtual DbSet<TblPreferredLanguage> TblPreferredLanguage { get; set; }
        public virtual DbSet<TblRewardAssigned> TblRewardAssigned { get; set; }
        public virtual DbSet<TblRewardCart> TblRewardCart { get; set; }
        public virtual DbSet<TblRewardCartProduct> TblRewardCartProduct { get; set; }
        public virtual DbSet<TblRewards> TblRewards { get; set; }
        public virtual DbSet<TblRewardType> TblRewardType { get; set; }
        public virtual DbSet<TblRole> TblRole { get; set; }
        public virtual DbSet<TblSentSmsresponse> TblSentSmsresponse { get; set; }
        public virtual DbSet<TblShare> TblShare { get; set; }
        public virtual DbSet<TblSharedByMobileNumber> TblSharedByMobileNumber { get; set; }
        public virtual DbSet<TblShareType> TblShareType { get; set; }
        public virtual DbSet<TblShoppingCart> TblShoppingCart { get; set; }
        public virtual DbSet<TblShoppingCartProduct> TblShoppingCartProduct { get; set; }
        public virtual DbSet<TblShoppingList> TblShoppingList { get; set; }
        public virtual DbSet<TblShoppingListProduct> TblShoppingListProduct { get; set; }
        public virtual DbSet<TblStatus> TblStatus { get; set; }
        public virtual DbSet<TblTab> TblTab { get; set; }
        public virtual DbSet<TblTabRolePermission> TblTabRolePermission { get; set; }
        public virtual DbSet<TblUser> TblUser { get; set; }
        public virtual DbSet<TblUserAddress> TblUserAddress { get; set; }
        public virtual DbSet<TblVersion> TblVersion { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblAdminUser>(entity =>
            {
                entity.HasKey(e => e.AdminUserId);

                entity.ToTable("tbl_AdminUser");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StatusId).HasDefaultValueSql("((1))");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TblAdminUser)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_AdminUser_tbl_Role");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblAdminUser)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_AdminUser_tbl_Status");
            });

            modelBuilder.Entity<TblAdvertiser>(entity =>
            {
                entity.HasKey(e => e.AdvertiserId);

                entity.ToTable("tbl_Advertiser");

                entity.Property(e => e.CompanyName).HasMaxLength(250);

                entity.Property(e => e.PersonName).HasMaxLength(250);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.HasOne(d => d.AdvertiserType)
                    .WithMany(p => p.TblAdvertiser)
                    .HasForeignKey(d => d.AdvertiserTypeId)
                    .HasConstraintName("FK_tbl_Advertiser_tbl_AdvertiserType");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblAdvertiser)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_tbl_Advertiser_tbl_Status");
            });

            modelBuilder.Entity<TblAdvertiserType>(entity =>
            {
                entity.HasKey(e => e.AdvertiserTypeId);

                entity.ToTable("tbl_AdvertiserType");

                entity.Property(e => e.AdvertiserType).HasMaxLength(150);
            });

            modelBuilder.Entity<TblAdvertisment>(entity =>
            {
                entity.HasKey(e => e.AdvertismentId);

                entity.ToTable("tbl_Advertisment");

                entity.Property(e => e.AdvImageUrl).HasMaxLength(250);

                entity.Property(e => e.ClickActionUrl).HasMaxLength(250);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.Advertiser)
                    .WithMany(p => p.TblAdvertisment)
                    .HasForeignKey(d => d.AdvertiserId)
                    .HasConstraintName("FK_tbl_Advertisment_tbl_Advertiser");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblAdvertisment)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_tbl_Advertisment_tbl_Status");
            });

            modelBuilder.Entity<TblAdvertismentCreates>(entity =>
            {
                entity.HasKey(e => e.AdvertismentCreatesId);

                entity.ToTable("tbl_AdvertismentCreates");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndAge).HasMaxLength(150);

                entity.Property(e => e.StartAge).HasMaxLength(150);

                entity.HasOne(d => d.Advertisment)
                    .WithMany(p => p.TblAdvertismentCreates)
                    .HasForeignKey(d => d.AdvertismentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_AdvertismentCreates_tbl_Advertisment");
            });

            modelBuilder.Entity<TblAdvertismentLocation>(entity =>
            {
                entity.HasKey(e => e.AdvertismentLocationId);

                entity.ToTable("tbl_AdvertismentLocation");

                entity.Property(e => e.AdvertismentLocationName).HasMaxLength(150);
            });

            modelBuilder.Entity<TblAdvertismentStatus>(entity =>
            {
                entity.HasKey(e => e.AdvertismentStatusId);

                entity.ToTable("tbl_AdvertismentStatus");

                entity.Property(e => e.HitDate).HasColumnType("datetime");

                entity.HasOne(d => d.Advertisement)
                    .WithMany(p => p.TblAdvertismentStatus)
                    .HasForeignKey(d => d.AdvertisementId)
                    .HasConstraintName("FK_tbl_AdvertismentStatus_tbl_Advertisment");

                entity.HasOne(d => d.AdvertismentLocation)
                    .WithMany(p => p.TblAdvertismentStatus)
                    .HasForeignKey(d => d.AdvertismentLocationId)
                    .HasConstraintName("FK__tbl_Adver__Adver__37C5420D");
            });

            modelBuilder.Entity<TblArea>(entity =>
            {
                entity.HasKey(e => e.AreaId);

                entity.ToTable("tbl_Area");

                entity.Property(e => e.AreaNameAr).HasMaxLength(50);

                entity.Property(e => e.AreaNameEn).HasMaxLength(50);
            });

            modelBuilder.Entity<TblChangedBy>(entity =>
            {
                entity.HasKey(e => e.ChangedById);

                entity.ToTable("tbl_ChangedBy");

                entity.Property(e => e.ChangedByName).HasMaxLength(150);
            });

            modelBuilder.Entity<TblCollectionStatus>(entity =>
            {
                entity.HasKey(e => e.CollectionStatusId);

                entity.ToTable("tbl_CollectionStatus");

                entity.Property(e => e.CollectionStatusId).ValueGeneratedNever();

                entity.Property(e => e.CollectionType).HasColumnType("nchar(10)");
            });

            modelBuilder.Entity<TblDeviceType>(entity =>
            {
                entity.HasKey(e => e.DeviceTypeId);

                entity.ToTable("tbl_DeviceType");

                entity.Property(e => e.DeviceTypeName).HasMaxLength(100);
            });

            modelBuilder.Entity<TblGender>(entity =>
            {
                entity.HasKey(e => e.GenderId);

                entity.ToTable("tbl_Gender");

                entity.Property(e => e.GenderName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblHouseType>(entity =>
            {
                entity.HasKey(e => e.HouseTypeId);

                entity.ToTable("tbl_HouseType");

                entity.Property(e => e.HouseTypeAr).HasMaxLength(100);

                entity.Property(e => e.HouseTypeEn).HasMaxLength(100);
            });

            modelBuilder.Entity<TblMessage>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.ToTable("tbl_Message");

                entity.Property(e => e.MessageAr).HasMaxLength(500);

                entity.Property(e => e.MessageEn).HasMaxLength(500);

                entity.Property(e => e.MessageKey).HasMaxLength(100);
            });

            modelBuilder.Entity<TblNationality>(entity =>
            {
                entity.HasKey(e => e.NationalityId);

                entity.ToTable("tbl_Nationality");

                entity.Property(e => e.NationalityNameAr).HasMaxLength(50);

                entity.Property(e => e.NationalityNameEn).HasMaxLength(50);
            });

            modelBuilder.Entity<TblNotification>(entity =>
            {
                entity.HasKey(e => e.NotificationId);

                entity.ToTable("tbl_Notification");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Message).HasMaxLength(250);

                entity.Property(e => e.MessageAr).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.NotifyTypeId).HasColumnName("NotifyTypeID");

                entity.Property(e => e.OrdeNumberOruserName)
                    .HasColumnName("OrdeNumberORUserName")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.TblNotification)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_Notification_tbl_NotificationType");
            });

            modelBuilder.Entity<TblNotificationType>(entity =>
            {
                entity.ToTable("tbl_NotificationType");

                entity.Property(e => e.Name).HasMaxLength(250);
            });

            modelBuilder.Entity<TblOrderAddress>(entity =>
            {
                entity.HasKey(e => e.OrderAddressId);

                entity.ToTable("tbl_OrderAddress");

                entity.Property(e => e.AreaName).HasMaxLength(250);

                entity.Property(e => e.Avenue).HasMaxLength(250);

                entity.Property(e => e.Block).HasMaxLength(250);

                entity.Property(e => e.HouseNumber).HasMaxLength(250);

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.Street).HasMaxLength(250);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.TblOrderAddress)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK__tbl_Order__Order__2E3BD7D3");
            });

            modelBuilder.Entity<TblOrderChangeType>(entity =>
            {
                entity.HasKey(e => e.OrderChangeId);

                entity.ToTable("tbl_OrderChangeType");

                entity.Property(e => e.ChangeName).HasMaxLength(150);
            });

            modelBuilder.Entity<TblOrderLogged>(entity =>
            {
                entity.HasKey(e => e.OrderLoggedId);

                entity.ToTable("tbl_OrderLogged");

                entity.Property(e => e.ChangedDateTme).HasColumnType("datetime");

                entity.Property(e => e.OrderAmount).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.PackageNameAr).HasMaxLength(150);

                entity.Property(e => e.PackageNameEn).HasMaxLength(150);

                entity.Property(e => e.SubTotal).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.UnitAr).HasMaxLength(150);

                entity.Property(e => e.UnitEn).HasMaxLength(150);

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.UnitValue).HasColumnType("decimal(18, 3)");
            });

            modelBuilder.Entity<TblOrders>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.ToTable("tbl_Orders");

                entity.Property(e => e.GrandTotals).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.GroceryFees).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.InvoiceNumber).HasMaxLength(50);

                entity.Property(e => e.MembershipId).HasMaxLength(150);

                entity.Property(e => e.OrderAmount).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.OrderStatusNote).HasMaxLength(500);

                entity.Property(e => e.OrederCreateDate).HasColumnType("datetime");

                entity.Property(e => e.QoutFees).HasColumnType("decimal(18, 3)");

                entity.HasOne(d => d.OrderStatus)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.OrderStatusId)
                    .HasConstraintName("FK_tbl_Orders_tbl_OrderStatus");

                entity.HasOne(d => d.OrderType)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.OrderTypeId)
                    .HasConstraintName("FK_tbl_Orders_tbl_OrderType");

                entity.HasOne(d => d.RewardAssign)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.RewardAssignId)
                    .HasConstraintName("FK_tbl_Orders_tbl_RewardAssigned");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblOrders)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tbl_Orders_tbl_User");
            });

            modelBuilder.Entity<TblOrderStatus>(entity =>
            {
                entity.HasKey(e => e.OrderStatusId);

                entity.ToTable("tbl_OrderStatus");

                entity.Property(e => e.OrderStatusId).ValueGeneratedNever();

                entity.Property(e => e.OrderStatus).HasMaxLength(150);

                entity.Property(e => e.OrderStatusAr).HasMaxLength(150);
            });

            modelBuilder.Entity<TblOrderType>(entity =>
            {
                entity.HasKey(e => e.OrderTypeId);

                entity.ToTable("tbl_OrderType");

                entity.Property(e => e.OrderTypeId).ValueGeneratedNever();

                entity.Property(e => e.OrderType).HasMaxLength(50);

                entity.Property(e => e.OrderTypeAr).HasMaxLength(250);
            });

            modelBuilder.Entity<TblPreferredLanguage>(entity =>
            {
                entity.HasKey(e => e.PreferredLanguageId);

                entity.ToTable("tbl_PreferredLanguage");

                entity.Property(e => e.PreferredLanguageId).ValueGeneratedNever();

                entity.Property(e => e.PreferredLanguageName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblRewardAssigned>(entity =>
            {
                entity.HasKey(e => e.RewardAssigneId);

                entity.ToTable("tbl_RewardAssigned");

                entity.Property(e => e.AssignedOn).HasColumnType("datetime");

                entity.Property(e => e.ClaimOn).HasColumnType("datetime");

                entity.Property(e => e.RewardCode).HasMaxLength(150);

                entity.HasOne(d => d.Reward)
                    .WithMany(p => p.TblRewardAssigned)
                    .HasForeignKey(d => d.RewardId)
                    .HasConstraintName("FK_tbl_RewardAssigned_tbl_Rewards");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblRewardAssigned)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tbl_RewardAssigned_tbl_User");
            });

            modelBuilder.Entity<TblRewardCart>(entity =>
            {
                entity.HasKey(e => e.RewardCartId);

                entity.ToTable("tbl_RewardCart");

                entity.Property(e => e.CartName).IsRequired();

                entity.HasOne(d => d.Reward)
                    .WithMany(p => p.TblRewardCart)
                    .HasForeignKey(d => d.RewardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_RewardCart_tbl_Rewards");
            });

            modelBuilder.Entity<TblRewardCartProduct>(entity =>
            {
                entity.HasKey(e => e.RewardCartProductId);

                entity.ToTable("tbl_RewardCartProduct");

                entity.HasOne(d => d.RewardCart)
                    .WithMany(p => p.TblRewardCartProduct)
                    .HasForeignKey(d => d.RewardCartId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_RewardCartProduct_tbl_RewardCart");
            });

            modelBuilder.Entity<TblRewards>(entity =>
            {
                entity.HasKey(e => e.RewardId);

                entity.ToTable("tbl_Rewards");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.RewardCode).HasMaxLength(150);

                entity.Property(e => e.RewardDescription).HasMaxLength(250);

                entity.Property(e => e.RewardValue).HasMaxLength(250);

                entity.HasOne(d => d.RewardType)
                    .WithMany(p => p.TblRewards)
                    .HasForeignKey(d => d.RewardTypeId)
                    .HasConstraintName("FK_tbl_Rewards_tbl_RewardType");
            });

            modelBuilder.Entity<TblRewardType>(entity =>
            {
                entity.HasKey(e => e.RewarTypeId);

                entity.ToTable("tbl_RewardType");

                entity.Property(e => e.RewardType).HasMaxLength(50);
            });

            modelBuilder.Entity<TblRole>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("tbl_Role");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TblSentSmsresponse>(entity =>
            {
                entity.HasKey(e => e.ResponseId);

                entity.ToTable("tbl_SentSMSResponse");

                entity.Property(e => e.ResponseId).HasColumnName("ResponseID");

                entity.Property(e => e.MessageId).HasMaxLength(70);

                entity.Property(e => e.MobileNumber).HasMaxLength(10);

                entity.Property(e => e.NetPoints).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ResponseMessage).HasMaxLength(250);

                entity.Property(e => e.SendDateTime).HasColumnType("datetime");

                entity.Property(e => e.SendMessage).HasMaxLength(500);
            });

            modelBuilder.Entity<TblShare>(entity =>
            {
                entity.HasKey(e => e.ShareId);

                entity.ToTable("tbl_Share");

                entity.Property(e => e.CartIdListId).HasColumnName("CartId/ListId");

                entity.Property(e => e.ShareDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.ShareType)
                    .WithMany(p => p.TblShare)
                    .HasForeignKey(d => d.ShareTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_Share_tbl_ShareType");
            });

            modelBuilder.Entity<TblSharedByMobileNumber>(entity =>
            {
                entity.HasKey(e => e.ShareMobileId);

                entity.ToTable("tbl_SharedByMobileNumber");

                entity.Property(e => e.CartIdListId).HasColumnName("CartId/ListId");

                entity.Property(e => e.ShareDateTime).HasColumnType("datetime");

                entity.Property(e => e.ToMobileNumber)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.HasOne(d => d.ShareType)
                    .WithMany(p => p.TblSharedByMobileNumber)
                    .HasForeignKey(d => d.ShareTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_SharedByMobileNumber_tbl_ShareType");
            });

            modelBuilder.Entity<TblShareType>(entity =>
            {
                entity.HasKey(e => e.ShareTypeId);

                entity.ToTable("tbl_ShareType");

                entity.Property(e => e.ShareType)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TblShoppingCart>(entity =>
            {
                entity.HasKey(e => e.ShoppingCartId);

                entity.ToTable("tbl_ShoppingCart");

                entity.Property(e => e.ShoppingCartName).HasMaxLength(250);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblShoppingCart)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tbl_ShoppingCart_tbl_User");
            });

            modelBuilder.Entity<TblShoppingCartProduct>(entity =>
            {
                entity.HasKey(e => e.ShoppingCartProductId);

                entity.ToTable("tbl_ShoppingCartProduct");

                entity.Property(e => e.AddedOn).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.PackageNameAr).HasMaxLength(150);

                entity.Property(e => e.PackageNameEn).HasMaxLength(150);

                entity.Property(e => e.SubTotal).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.UnitAr).HasMaxLength(150);

                entity.Property(e => e.UnitEn).HasMaxLength(150);

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.UnitValue).HasColumnType("decimal(18, 3)");

                entity.HasOne(d => d.CollectionStatus)
                    .WithMany(p => p.TblShoppingCartProduct)
                    .HasForeignKey(d => d.CollectionStatusId)
                    .HasConstraintName("FK_tbl_ShoppingCartProduct_tbl_CollectionStatus");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.TblShoppingCartProduct)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_tbl_ShoppingCartProduct_tbl_Orders");

                entity.HasOne(d => d.ShoppingCart)
                    .WithMany(p => p.TblShoppingCartProduct)
                    .HasForeignKey(d => d.ShoppingCartId)
                    .HasConstraintName("FK_tbl_ShoppingCartProduct_tbl_ShoppingCart");
            });

            modelBuilder.Entity<TblShoppingList>(entity =>
            {
                entity.HasKey(e => e.ShoppingListId);

                entity.ToTable("tbl_ShoppingList");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ShoppingName).HasMaxLength(250);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblShoppingList)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tbl_ShoppingList_tbl_User");
            });

            modelBuilder.Entity<TblShoppingListProduct>(entity =>
            {
                entity.HasKey(e => e.ShoppingListProductId);

                entity.ToTable("tbl_ShoppingListProduct");

                entity.Property(e => e.AddedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ShoppingList)
                    .WithMany(p => p.TblShoppingListProduct)
                    .HasForeignKey(d => d.ShoppingListId)
                    .HasConstraintName("FK_tbl_ShoppingListProduct_tbl_ShoppingList");
            });

            modelBuilder.Entity<TblStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("tbl_Status");

                entity.Property(e => e.Status).HasMaxLength(50);
            });

            modelBuilder.Entity<TblTab>(entity =>
            {
                entity.HasKey(e => e.TabId);

                entity.ToTable("tbl_Tab");

                entity.Property(e => e.TabName)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TblTabRolePermission>(entity =>
            {
                entity.HasKey(e => e.TabRoleId);

                entity.ToTable("tbl_TabRolePermission");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Tab)
                    .WithMany(p => p.TblTabRolePermission)
                    .HasForeignKey(d => d.TabId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_TabRolePermission_tbl_Tab1");
            });

            modelBuilder.Entity<TblUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("tbl_User");

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeviceToken).HasMaxLength(500);

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.FirstName).HasMaxLength(150);

                entity.Property(e => e.LastName).HasMaxLength(150);

                entity.Property(e => e.MobileNumber).HasMaxLength(150);

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.NotifyLanguageId).HasColumnName("NotifyLanguageID");

                entity.Property(e => e.Password).HasMaxLength(150);

                entity.Property(e => e.UserName).HasMaxLength(150);

                entity.HasOne(d => d.DeviceType)
                    .WithMany(p => p.TblUser)
                    .HasForeignKey(d => d.DeviceTypeId)
                    .HasConstraintName("FK__tbl_User__Device__4CC05EF3");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.TblUser)
                    .HasForeignKey(d => d.GenderId)
                    .HasConstraintName("FK_tbl_User_tbl_Gender");

                entity.HasOne(d => d.PreferredLanguage)
                    .WithMany(p => p.TblUser)
                    .HasForeignKey(d => d.PreferredLanguageId)
                    .HasConstraintName("FK_tbl_User_tbl_PreferredLanguage");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblUser)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_tbl_User_tbl_Status");
            });

            modelBuilder.Entity<TblUserAddress>(entity =>
            {
                entity.HasKey(e => e.UserAddressId);

                entity.ToTable("tbl_UserAddress");

                entity.Property(e => e.Avenue).HasMaxLength(250);

                entity.Property(e => e.Block).HasMaxLength(250);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HouseNumber).HasMaxLength(250);

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.Street).HasMaxLength(250);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblUserAddress)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tbl_UserAddress_tbl_User");
            });

            modelBuilder.Entity<TblVersion>(entity =>
            {
                entity.HasKey(e => e.VersionId);

                entity.ToTable("tbl_Version");

                entity.Property(e => e.AddedDate).HasColumnType("datetime");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
