﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblShoppingCart : BaseEntity
    {
        public TblShoppingCart()
        {
            TblShoppingCartProduct = new HashSet<TblShoppingCartProduct>();
        }

        public int ShoppingCartId { get; set; }
        public string ShoppingCartName { get; set; }
        public int? UserId { get; set; }

        public TblUser User { get; set; }
        public ICollection<TblShoppingCartProduct> TblShoppingCartProduct { get; set; }
    }
}
