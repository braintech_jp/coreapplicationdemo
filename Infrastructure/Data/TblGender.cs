﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;


namespace Infrastructure.Data
{
    public partial class TblGender : BaseEntity
    {
        public TblGender()
        {
            TblUser = new HashSet<TblUser>();
        }

        public int GenderId { get; set; }
        public string GenderName { get; set; }

        public ICollection<TblUser> TblUser { get; set; }
    }
}
