﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblNationality : BaseEntity
    {
        public int NationalityId { get; set; }
        public string NationalityNameEn { get; set; }
        public string NationalityNameAr { get; set; }
    }
}
