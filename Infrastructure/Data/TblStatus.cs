﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblStatus : BaseEntity
    {
        public TblStatus()
        {
            TblAdminUser = new HashSet<TblAdminUser>();
            TblAdvertiser = new HashSet<TblAdvertiser>();
            TblAdvertisment = new HashSet<TblAdvertisment>();
            TblUser = new HashSet<TblUser>();
        }

        public int StatusId { get; set; }
        public string Status { get; set; }

        public ICollection<TblAdminUser> TblAdminUser { get; set; }
        public ICollection<TblAdvertiser> TblAdvertiser { get; set; }
        public ICollection<TblAdvertisment> TblAdvertisment { get; set; }
        public ICollection<TblUser> TblUser { get; set; }
    }
}
