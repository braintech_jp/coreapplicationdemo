﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblShoppingList : BaseEntity
    {
        public TblShoppingList()
        {
            TblShoppingListProduct = new HashSet<TblShoppingListProduct>();
        }

        public int ShoppingListId { get; set; }
        public int? UserId { get; set; }
        public string ShoppingName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? IsDeleted { get; set; }

        public TblUser User { get; set; }
        public ICollection<TblShoppingListProduct> TblShoppingListProduct { get; set; }
    }
}
