﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblOrderAddress : BaseEntity
    {
        public int OrderAddressId { get; set; }
        public int? OrderId { get; set; }
        public string Block { get; set; }
        public string Avenue { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string Notes { get; set; }
        public string AreaName { get; set; }
        public int? HouseTypeId { get; set; }

        public TblOrders Order { get; set; }
    }
}
