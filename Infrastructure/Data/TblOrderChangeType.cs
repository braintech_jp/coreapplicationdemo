﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblOrderChangeType : BaseEntity
    {
        public int OrderChangeId { get; set; }
        public string ChangeName { get; set; }
    }
}
