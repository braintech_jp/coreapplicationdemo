﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblPreferredLanguage : BaseEntity
    {
        public TblPreferredLanguage()
        {
            TblUser = new HashSet<TblUser>();
        }

        public int PreferredLanguageId { get; set; }
        public string PreferredLanguageName { get; set; }

        public ICollection<TblUser> TblUser { get; set; }
    }
}
