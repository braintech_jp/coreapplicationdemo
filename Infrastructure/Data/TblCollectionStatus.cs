﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblCollectionStatus : BaseEntity
    {
        public TblCollectionStatus()
        {
            TblShoppingCartProduct = new HashSet<TblShoppingCartProduct>();
        }

        public int CollectionStatusId { get; set; }
        public string CollectionType { get; set; }

        public ICollection<TblShoppingCartProduct> TblShoppingCartProduct { get; set; }
    }
}
