﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblOrderLogged : BaseEntity
    {
        public int OrderLoggedId { get; set; }
        public int? OrderId { get; set; }
        public int? ChangedById { get; set; }
        public int? ChangedUserId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public DateTime? ChangedDateTme { get; set; }
        public decimal? UnitPrice { get; set; }
        public string UnitAr { get; set; }
        public string UnitEn { get; set; }
        public string PackageNameAr { get; set; }
        public string PackageNameEn { get; set; }
        public decimal? UnitValue { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? OrderAmount { get; set; }
    }
}
