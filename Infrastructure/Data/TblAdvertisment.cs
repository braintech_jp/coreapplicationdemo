﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdvertisment : BaseEntity
    {
        public TblAdvertisment()
        {
            TblAdvertismentCreates = new HashSet<TblAdvertismentCreates>();
            TblAdvertismentStatus = new HashSet<TblAdvertismentStatus>();
        }

        public int AdvertismentId { get; set; }
        public string AdvImageUrl { get; set; }
        public string ClickActionUrl { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? AdvertiserId { get; set; }
        public int? IsDeleted { get; set; }
        public int? StatusId { get; set; }

        public TblAdvertiser Advertiser { get; set; }
        public TblStatus Status { get; set; }
        public ICollection<TblAdvertismentCreates> TblAdvertismentCreates { get; set; }
        public ICollection<TblAdvertismentStatus> TblAdvertismentStatus { get; set; }
    }
}
