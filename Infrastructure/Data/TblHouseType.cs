﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblHouseType : BaseEntity
    {
        public int HouseTypeId { get; set; }
        public string HouseTypeEn { get; set; }
        public string HouseTypeAr { get; set; }
    }
}
