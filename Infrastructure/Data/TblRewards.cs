﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblRewards : BaseEntity
    {
        public TblRewards()
        {
            TblRewardAssigned = new HashSet<TblRewardAssigned>();
            TblRewardCart = new HashSet<TblRewardCart>();
        }

        public int RewardId { get; set; }
        public int? RewardTypeId { get; set; }
        public string RewardValue { get; set; }
        public string RewardDescription { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? IsDeleted { get; set; }
        public string RewardCode { get; set; }

        public TblRewardType RewardType { get; set; }
        public ICollection<TblRewardAssigned> TblRewardAssigned { get; set; }
        public ICollection<TblRewardCart> TblRewardCart { get; set; }
    }
}
