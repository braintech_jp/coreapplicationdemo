﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;


namespace Infrastructure.Data
{
    public partial class TblTab : BaseEntity
    {
        public TblTab()
        {
            TblTabRolePermission = new HashSet<TblTabRolePermission>();
        }

        public int TabId { get; set; }
        public string TabName { get; set; }

        public ICollection<TblTabRolePermission> TblTabRolePermission { get; set; }
    }
}
