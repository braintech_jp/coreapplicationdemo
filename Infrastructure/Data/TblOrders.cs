﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblOrders : BaseEntity
    {
        public TblOrders()
        {
            TblOrderAddress = new HashSet<TblOrderAddress>();
            TblShoppingCartProduct = new HashSet<TblShoppingCartProduct>();
        }

        public int OrderId { get; set; }
        public decimal? OrderAmount { get; set; }
        public int? UserId { get; set; }
        public int? GroceryId { get; set; }
        public int? OrderTypeId { get; set; }
        public int? OrderStatusId { get; set; }
        public string OrderStatusNote { get; set; }
        public int? RewardAssignId { get; set; }
        public DateTime? OrederCreateDate { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal? GroceryFees { get; set; }
        public decimal? QoutFees { get; set; }
        public decimal? GrandTotals { get; set; }
        public string MembershipId { get; set; }
        public bool? IsComplete { get; set; }
        public bool? IsDeleted { get; set; }

        public TblOrderStatus OrderStatus { get; set; }
        public TblOrderType OrderType { get; set; }
        public TblRewardAssigned RewardAssign { get; set; }
        public TblUser User { get; set; }
        public ICollection<TblOrderAddress> TblOrderAddress { get; set; }
        public ICollection<TblShoppingCartProduct> TblShoppingCartProduct { get; set; }
    }
}
