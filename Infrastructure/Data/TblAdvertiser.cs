﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdvertiser : BaseEntity
    {
        public TblAdvertiser()
        {
            TblAdvertisment = new HashSet<TblAdvertisment>();
        }

        public int AdvertiserId { get; set; }
        public string PersonName { get; set; }
        public string PhoneNumber { get; set; }
        public int? AdvertiserTypeId { get; set; }
        public string CompanyName { get; set; }
        public int? StatusId { get; set; }
        public int? IsDeleted { get; set; }

        public TblAdvertiserType AdvertiserType { get; set; }
        public TblStatus Status { get; set; }
        public ICollection<TblAdvertisment> TblAdvertisment { get; set; }
    }
}
