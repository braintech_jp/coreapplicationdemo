﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblRewardAssigned : BaseEntity
    {
        public TblRewardAssigned()
        {
            TblOrders = new HashSet<TblOrders>();
        }

        public int RewardAssigneId { get; set; }
        public int? UserId { get; set; }
        public int? RewardId { get; set; }
        public DateTime? AssignedOn { get; set; }
        public DateTime? ClaimOn { get; set; }
        public int? ClaimGrocery { get; set; }
        public int? OrderId { get; set; }
        public bool? IsClaimed { get; set; }
        public string RewardCode { get; set; }

        public TblRewards Reward { get; set; }
        public TblUser User { get; set; }
        public ICollection<TblOrders> TblOrders { get; set; }
    }
}
