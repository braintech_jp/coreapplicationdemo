﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdminUser: BaseEntity
    {
        public int AdminUserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public int StatusId { get; set; }
        public int IsDelete { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Email { get; set; }
        public int? GroceryId { get; set; }

        public TblRole Role { get; set; }
        public TblStatus Status { get; set; }
    }
}
