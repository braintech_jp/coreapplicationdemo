﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdvertismentLocation : BaseEntity
    {
        public TblAdvertismentLocation()
        {
            TblAdvertismentStatus = new HashSet<TblAdvertismentStatus>();
        }

        public int AdvertismentLocationId { get; set; }
        public string AdvertismentLocationName { get; set; }

        public ICollection<TblAdvertismentStatus> TblAdvertismentStatus { get; set; }
    }
}
