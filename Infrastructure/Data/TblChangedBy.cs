﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblChangedBy : BaseEntity
    {
        public int ChangedById { get; set; }
        public string ChangedByName { get; set; }
    }
}
