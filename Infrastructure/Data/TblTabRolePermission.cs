﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblTabRolePermission : BaseEntity
    {
        public int TabRoleId { get; set; }
        public int TabId { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }

        public TblTab Tab { get; set; }
    }
}
