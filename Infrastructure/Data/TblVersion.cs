﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblVersion : BaseEntity
    {
        public int VersionId { get; set; }
        public int DeviceTypeId { get; set; }
        public string Version { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
