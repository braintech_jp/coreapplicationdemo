﻿using System;
using System.Collections.Generic;
using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblAdvertiserType : BaseEntity
    {
        public TblAdvertiserType()
        {
            TblAdvertiser = new HashSet<TblAdvertiser>();
        }

        public int AdvertiserTypeId { get; set; }
        public string AdvertiserType { get; set; }

        public ICollection<TblAdvertiser> TblAdvertiser { get; set; }
    }
}
