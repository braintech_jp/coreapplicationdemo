﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblOrderStatus : BaseEntity
    {
        public TblOrderStatus()
        {
            TblOrders = new HashSet<TblOrders>();
        }

        public int OrderStatusId { get; set; }
        public string OrderStatus { get; set; }
        public string OrderStatusAr { get; set; }

        public ICollection<TblOrders> TblOrders { get; set; }
    }
}
