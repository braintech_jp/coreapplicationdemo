﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblDeviceType : BaseEntity
    {
        public TblDeviceType()
        {
            TblUser = new HashSet<TblUser>();
        }

        public int DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; }

        public ICollection<TblUser> TblUser { get; set; }
    }
}
