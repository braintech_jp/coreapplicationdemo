﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblSharedByMobileNumber : BaseEntity
    {
        public int ShareMobileId { get; set; }
        public int FromUserId { get; set; }
        public string ToMobileNumber { get; set; }
        public DateTime ShareDateTime { get; set; }
        public int ShareTypeId { get; set; }
        public int? CartIdListId { get; set; }

        public TblShareType ShareType { get; set; }
    }
}
