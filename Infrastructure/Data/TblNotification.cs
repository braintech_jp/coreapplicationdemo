﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblNotification : BaseEntity
    {
        public int NotificationId { get; set; }
        public int TypeId { get; set; }
        public string Message { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int? FromUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int IsRead { get; set; }
        public string MessageAr { get; set; }
        public string OrdeNumberOruserName { get; set; }
        public int? NotifyTypeId { get; set; }

        public TblNotificationType Type { get; set; }
    }
}
