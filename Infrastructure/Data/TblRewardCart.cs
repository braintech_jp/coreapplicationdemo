﻿using System;
using System.Collections.Generic;

using Infrastructure.Entity;

namespace Infrastructure.Data
{
    public partial class TblRewardCart : BaseEntity
    {
        public TblRewardCart()
        {
            TblRewardCartProduct = new HashSet<TblRewardCartProduct>();
        }

        public int RewardCartId { get; set; }
        public string CartName { get; set; }
        public int RewardId { get; set; }
        public int IsDeleted { get; set; }

        public TblRewards Reward { get; set; }
        public ICollection<TblRewardCartProduct> TblRewardCartProduct { get; set; }
    }
}
