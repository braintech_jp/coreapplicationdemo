﻿using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Specifications
{
   public  class UserSpecification : BaseSpecification<TblUser>
    {

        public UserSpecification()
            : base(x => (x.IsDeleted ??0) ==  0 && (x.IsGuestUser ??0) == 0 && (x.StatusId ??0) == 1)
        {

        }
    }
}
