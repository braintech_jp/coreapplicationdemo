﻿using ApplicationCore.Helper;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Specifications
{
    public class CreateUserSpecifiction : BaseSpecification<TblUser>
    {
        public CreateUserSpecifiction(string mobileNumber, string userName, string EmailAddress)
            : base((x => x.UserName == userName || x.MobileNumber == mobileNumber || x.Email == EmailAddress &&( x.IsDeleted == 0)))
        {

        }
        public CreateUserSpecifiction(int userID) :
          base(x => (x.IsDeleted ?? 0) == 0 && (x.StatusId ?? 0) == (int)Enumeration.Status.Active
          && x.UserId == userID)
        {

        }


    }
}
