﻿using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Specifications
{
    public class LoginSpecification : BaseSpecification<TblAdminUser>
    {
        public LoginSpecification(string userName, string password)
            : base(x => x.UserName == userName && x.Password == password)
        {

        }
    }
}
