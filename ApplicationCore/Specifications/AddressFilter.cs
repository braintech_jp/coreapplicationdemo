﻿using ApplicationCore.Specifications;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Specification
{
    public class AddressFilter : BaseSpecification<TblUserAddress>
    {
        public AddressFilter(int userID) :
          base(x => (x.IsDeleted ?? 0) == 0 && (x.DefaultAddress ?? false) == true && x.UserId == userID)
        {

        }
        public AddressFilter(int userID, int AddressID) :
          base(x => (x.IsDeleted ?? 0) == 0 && (x.DefaultAddress ?? false) == true && x.UserId == userID &&
                    x.UserAddressId == AddressID)
        {

        }
    }
}
