﻿using Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class AreaEntity : BaseEntity
    {
        public int Id { get; set; }

        public string AreaName { get; set; }

    }

    public class HouseTypeEntity : BaseEntity
    {
        public int Id { get; set; }

        public string HouseType { get; set; }

    }

    public class NationalityEntity : BaseEntity
    {
        public int Id { get; set; }
        public string Nationality { get; set; }
    }
}
