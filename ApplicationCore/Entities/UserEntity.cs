﻿using Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class UserEntity : BaseEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int AddressID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public int GenderId { get; set; }
        public DateTime BirthDate { get; set; }
        public int NationalityId { get; set; }
        public string Nationality { get; set; }
        public int AreaId { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Avenue { get; set; }
        public string Street { get; set; }
        public int HouseTypeId { get; set; }
        public string HouseType { get; set; }
        public string HouseNumber { get; set; }
        public string Notes { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public string Gender { get; set; }

        public List<RegistersModel> UserDetails { get; set; }
    }
    public class RegistersModel
    {
        public string Area { get; set; }

        public int AreaId { get; set; }

        public string Avenue { get; set; }

        public string BirthDate { get; set; }
        public string Block { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }
        public string Gender { get; set; }

        public int GenderId { get; set; }

        public string HouseNumber { get; set; }
        public string HouseType { get; set; }

        public int HouseTypeId { get; set; }

        public int IsActive { get; set; }
        public int IsDelete { get; set; }

        public string LastName { get; set; }

        public string MobileNumber { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }

        public int NationalityId { get; set; }


        public string Notes { get; set; }

        public string Password { get; set; }
        public string Street { get; set; }
        public int UserId { get; set; }

        public string UserName { get; set; }
    }
}
