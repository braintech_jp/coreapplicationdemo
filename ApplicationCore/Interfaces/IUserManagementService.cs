﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IUserManagementService
    {
        bool CreateNewUser(UserEntity model);

        List<UserEntity> GetAllUsersDetail();

        UserEntity GetUserDetail(int userId);
    }
}
