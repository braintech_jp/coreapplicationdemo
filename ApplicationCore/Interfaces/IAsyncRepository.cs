﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IAsyncRepository<T>
    {
        Task<T> GetById(int id);

        Task<List<T>> ListAllAsc();
        Task<T> GetDetails(ISpecification<T> spec);
        Task<T> Add(T entity);
       

        void Update(T Entity);
        void Delete(T Entity);
    }
}
