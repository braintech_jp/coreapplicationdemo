﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ICommonService
    {
        Task<List<AreaEntity>> UserAreaList();

        Task<List<NationalityEntity>> UserNationalityList();

        Task<List<HouseTypeEntity>> UserHouseTypeList();

    }

}
