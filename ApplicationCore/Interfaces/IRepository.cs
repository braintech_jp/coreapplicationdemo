﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IRepository<T>
    {
        T GetById(int id);
        IEnumerable<T> ListAll();
        IEnumerable<T> ListAllDetails(ISpecification<T> spec);
        T GetDetails(ISpecification<T> spec);
        T Add(T entity);
        void Update(T Entity);
        void Delete(T Entity);
        //T Set(T Entity);

    }
}
