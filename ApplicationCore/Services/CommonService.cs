﻿using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Infrastructure.Data;
using System.Linq;

namespace ApplicationCore.Services
{
    public class CommonService : ICommonService
    {
        private readonly IAsyncRepository<TblArea> _dbAreaRepository;
        private readonly IAsyncRepository<TblNationality> _dbNationalityRepository;
        private readonly IAsyncRepository<TblHouseType> _dbHouseTypeRepository;

        public CommonService(IAsyncRepository<TblArea> dbAreaRepository, IAsyncRepository<TblNationality> dbNationalityRepository, IAsyncRepository<TblHouseType> dbHouseTypeRepository)
        {
            _dbAreaRepository = dbAreaRepository;

            _dbNationalityRepository = dbNationalityRepository;


            _dbHouseTypeRepository = dbHouseTypeRepository;
        }

        //return (await _nationalityRepo.ListAllAsync()).Select(x => new NationalityEntity()
        //{
        //    NationalityID = x.NationalityId,
        //        NationalityName = x.NationalityNameEn
        //    }).ToList();

        public async Task<List<AreaEntity>> UserAreaList()
        {
            return (await _dbAreaRepository.ListAllAsc()).Select(x => new AreaEntity()

            {
                Id = x.AreaId,
                AreaName = x.AreaNameAr
            }
            ).ToList();

        }

      public async  Task<List<HouseTypeEntity>> UserHouseTypeList()
        {

            return (await _dbHouseTypeRepository.ListAllAsc()).Select(x => new HouseTypeEntity()
            {
               Id= x.HouseTypeId,

               HouseType=x.HouseTypeEn
               
            }
            ).ToList();

        }

       public async Task<List<NationalityEntity>> UserNationalityList()
        {
            return (await _dbNationalityRepository.ListAllAsc()).Select(x => new NationalityEntity
            {
                Id= x.NationalityId,

                Nationality=x.NationalityNameEn
            }
            ).ToList();
        }
    }
}
