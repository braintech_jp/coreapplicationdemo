﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Specifications;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    //private readonly IRepository<CatalogItem> _itemRepository;
    public class LoginService : ILoginService
    {
        private readonly IRepository<TblAdminUser> _dbRepository;
        public LoginService(IRepository<TblAdminUser> dbRepository)
        {
            _dbRepository = dbRepository;
        }
        public UserEntity UserLogin(UserEntity model)
        {
            LoginSpecification filter = new LoginSpecification(model.UserName, model.Password);

            var result =  _dbRepository.GetDetails(filter);

            if (result != null)
            {
                return new UserEntity()
                {
                    Id= result.AdminUserId,

                    Name = result.UserName,

                    Email = result.Email
                };
            }
            return null;
        }

    }
}
