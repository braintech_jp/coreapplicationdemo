﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using ApplicationCore.Entities;
using ApplicationCore.Specifications;
using System.Transactions;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Specification;

namespace ApplicationCore.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly IRepository<TblUser> _dbUserRepository;

        private readonly IRepository<TblUserAddress> _dbUserAddressRepository;

        public UserManagementService(IRepository<TblUser> dbUserRepository, IRepository<TblUserAddress> dbUserAddressRepository)
        {
            _dbUserRepository = dbUserRepository;

            _dbUserAddressRepository = dbUserAddressRepository;

        }

        public bool CreateNewUser(UserEntity model)
        {


            if (model.UserId == 0)
            {
                CreateUserSpecifiction filter = new CreateUserSpecifiction(model.MobileNumber, model.UserName, model.Email);

                var result = _dbUserRepository.GetDetails(filter);

                TblUser objUser = new TblUser()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    UserName = model.UserName,
                    Password = model.Password,
                    Email = model.Email,
                    NationalityId = model.NationalityId,
                    GenderId = model.GenderId,
                    BirthDate = model.BirthDate,
                    MobileNumber = model.MobileNumber,
                    CreatedOn = System.DateTime.Now,
                    IsDeleted = 0,
                    StatusId = 1,
                    Rating = 0,
                    PreferredLanguageId = 1,
                    IsGuestUser = 0,
                    IsAdvertismentShow = 1,

                };
                _dbUserRepository.Add(objUser);

                TblUserAddress objUserAddress = new TblUserAddress()
                {
                    UserId = objUser.UserId,
                    AreaId = model.AreaId,
                    Block = model.Block,
                    Avenue = model.Avenue,
                    Street = model.Street,
                    HouseTypeId = model.HouseTypeId,
                    HouseNumber = model.HouseNumber,
                    CreatedOn = System.DateTime.Now,
                    IsDeleted = 0,
                    Notes = model.Notes,
                    DefaultAddress = true
                };

                _dbUserAddressRepository.Add(objUserAddress);

                return true;


            }
            else if (model.UserId > 0 && model.AddressID > 0)
            {
                CreateUserSpecifiction filterUser = new CreateUserSpecifiction(model.UserId);

                var resultUser = _dbUserRepository.GetDetails(filterUser);

                resultUser.UserName = model.UserName;

                if (model.Password != null)
                {
                    resultUser.Password = model.Password;
                }
                resultUser.FirstName = model.FirstName;
                resultUser.LastName = model.LastName;
                resultUser.Email = model.Email;
                resultUser.NationalityId = model.NationalityId;
                resultUser.GenderId = model.GenderId;
                resultUser.MobileNumber = model.MobileNumber;
                resultUser.BirthDate = model.BirthDate;

                _dbUserRepository.Update(resultUser);

                AddressFilter addfilter = new AddressFilter(model.UserId, model.AddressID);

                var res = _dbUserAddressRepository.GetDetails(addfilter);
                if (res != null)
                {
                    res.AreaId = model.AreaId;
                    res.Block = model.Block;
                    res.Avenue = model.Avenue;
                    res.Street = model.Street;
                    res.HouseTypeId = model.HouseTypeId;
                    res.HouseNumber = model.HouseNumber;
                    res.Notes = model.Notes;
                    _dbUserAddressRepository.Update(res);
                }

                return true;
            }

            else
            {
                return false;
            }



        }

        public UserEntity GetUserDetail(int userId)
        {
            CreateUserSpecifiction filter = new CreateUserSpecifiction(userId);
            var userDetail = _dbUserRepository.GetDetails(filter);
            if (userDetail != null)
            {
                AddressFilter addFilter = new AddressFilter(userId);
                var address = _dbUserAddressRepository.GetDetails(addFilter);
                if (address != null)
                {
                    return new UserEntity()
                    {
                        UserId = userDetail.UserId,
                        FirstName = userDetail.FirstName,
                        LastName = userDetail.LastName,
                        UserName = userDetail.UserName,
                        Password = userDetail.Password,
                        Email = userDetail.Email,
                        NationalityId = userDetail.NationalityId ?? 0,
                        GenderId = userDetail.GenderId ?? 0,
                        MobileNumber = userDetail.MobileNumber,
                        BirthDate = userDetail.BirthDate ?? DateTime.Now,
                        AddressID = address.UserAddressId,
                        AreaId = address.AreaId ?? 0,
                        Block = address.Block,
                        Avenue = address.Avenue,
                        Street = address.Street,
                        HouseTypeId = address.HouseTypeId ?? 0,
                        HouseNumber = address.HouseNumber,
                        Notes = address.Notes,
                    };
                }
            }
            return null;
        }


        public List<UserEntity> GetAllUsersDetail()
        {
            UserSpecification filter = new UserSpecification();

            var result = _dbUserRepository.ListAllDetails(filter);

            return result.Select(x => new UserEntity()
            {
                UserId = x.UserId,
                Name = x.FirstName + " " + x.LastName,
                UserName = x.UserName,
                //Password = objCommon.Decrypt(x.Password),//before retrieve pwd it decrypt the pwd
                MobileNumber = x.MobileNumber,
                Email = x.Email
            }).ToList();
        }
    }
}
