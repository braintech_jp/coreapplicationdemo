﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Infrastructure.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class EFRepositoryService<T> : IAsyncRepository<T>, IRepository<T> where T : BaseEntity
    {
        private readonly QuotGroceryContext _dbContext;
        public EFRepositoryService(QuotGroceryContext dbContext) => _dbContext = dbContext;

        public T GetById(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }



        public IEnumerable<T> ListAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public IEnumerable<T> ListAllDetails(ISpecification<T> spec)
        {
            return _dbContext.Set<T>().Where(spec.Criteria).ToList();
        }

        public T GetDetails(ISpecification<T> spec)
        {
            return _dbContext.Set<T>().Where(spec.Criteria).FirstOrDefault();
        }
        public T Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Update(T Entity)
        {
            _dbContext.Entry(Entity).State = EntityState.Modified;
           // _dbContext.Set<T>().Add(Entity);
            _dbContext.SaveChanges();
        }
        public void Delete(T Entity)
        {
            _dbContext.Set<T>().Remove(Entity);
            _dbContext.SaveChanges();
        }

        Task<T> IAsyncRepository<T>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<T>> ListAllAsc()
        {
            return await _dbContext.Set<T>().ToListAsync();

        }

        Task<T> IAsyncRepository<T>.GetDetails(ISpecification<T> spec)
        {
            throw new NotImplementedException();
        }

        Task<T> IAsyncRepository<T>.Add(T entity)
        {
            throw new NotImplementedException();
        }

        //public async Task<T> GetById(int id)
        //{
        //    return await _dbContext.Set<T>().Find(id);
        //}

        //public IEnumerable<T> ListAll()
        //{
        //    return _dbContext.Set<T>().ToList();
        //}
        //public T GetDetails(ISpecification<T> spec)
        //{
        //    return _dbContext.Set<T>().Where(spec.Criteria).FirstOrDefault();
        //}
        //public T Add(T entity)
        //{
        //    _dbContext.Set<T>().Add(entity);
        //    _dbContext.SaveChanges();
        //    return entity;
        //}

        //public void Update(T Entity)
        //{
        //    _dbContext.Entry(Entity).State = EntityState.Modified;
        //    _dbContext.Set<T>().Add(Entity);
        //    _dbContext.SaveChanges();
        //}
        //public void Delete(T Entity)
        //{
        //    _dbContext.Set<T>().Remove(Entity);
        //    _dbContext.SaveChanges();
        //}
    }
}
