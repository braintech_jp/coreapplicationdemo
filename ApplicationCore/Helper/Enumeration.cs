﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Helper
{
    public static class Enumeration
    {
        public enum Gender
        {
            Male = 1,
            Female = 2
        }
        public enum Status
        {
            Active = 1,
            Inactive = 2
        }

        public enum Language
        {
            English = 1,
            Arabic = 2
        }
    }
}
